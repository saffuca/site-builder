import { App } from './classes/app'
import './styles/main.css'
import { model } from './model'

new App(model)